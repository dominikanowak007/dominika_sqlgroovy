@Grapes(
        @Grab(group='org.apache.commons', module='commons-lang3', version='3.7')
)

import  org.apache.commons.lang3.*

class Helpers {
    static String getRandomStringValue()
    {
        RandomStringUtils.random(7,true,false)

    }

   static int getRandomInt(x,y)
    {
        RandomUtils.nextInt(x,y)
    }

}
