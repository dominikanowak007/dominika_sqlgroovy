import groovy.sql.Sql

class GroovySQLApp {

    void welcome(){

        println("******************************  Welcome in Groovy SQL Application  **************************")
    }

    void db_script(){

        println("******************************  Starting SQL DB Connection **************************")

        def sql = Sql.newInstance("jdbc:mysql://localhost:3306/dom", "root","Ropucha007", "com.mysql.jdbc.Driver")
        createTables(sql)
        println("****************************** Tables Populated  **************************")
        populateTables(sql)
        println("****************************** SQL Query 1 **************************")
        executeQuery1(sql)
        println("****************************** SQL Query 2 **************************")
        executeQuery2(sql)
        println("****************************** SQL Query 3 **************************")
        executeQuery3(sql)
        println("****************************** SQL Query 3 **************************")
        executeQuery4(sql)
        println("******************************  Starting SQL Task Six **************************")
       // executeTask6(sql) - creates and populates table with 1 million records
        println("****************************** SQL Coonection about to close  **************************")
        sql.close()
        println("******************************  SQL Connection is Closed **************************")



    }

    def createTables(Sql sql) {
        sql.execute('DROP TABLE IF EXISTS '+ 'table_a')
        sql.execute('DROP TABLE IF EXISTS '+ 'table_b')

        sql.execute('create table table_a(' +
                'id BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL,' +
                'name VARCHAR(255) NOT NULL,' +
                'some_number int(11) NOT NULL, ' +
                'some_boolean bit(1) NOT NULL )')

        sql.execute('create table table_b(' +
                'id BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL,' +
                'name VARCHAR(255) NOT NULL,' +
                'some_number int(11) NOT NULL,' +
                'some_boolean bit(1) NOT NULL )')
    }

    def populateTables(Sql sql)
    {
        boolean tak = false

        (1..100).each {


            String randomString = Helpers.getRandomStringValue()
            def params = [randomString, it , (tak? 1:0)]
            sql.execute 'insert into table_a  (name, some_number,some_boolean) values (?, ?, ?)', params


            def params2 = [randomString.reverse(), Helpers.getRandomInt(100,200) , (tak? 0:1)]
            sql.execute 'insert into table_b  (name, some_number,some_boolean) values (?, ?, ?)', params2

            tak =(!tak)
        }
    }



    def executeQuery1(Sql sql)
    {
    println'Starting Executing Query 1 - LEFT JOIN'
     def rows =   sql.rows( 'SELECT a.id AS aId, a.name AS aName, b.id AS bId, b.name AS bName from  table_a AS a LEFT JOIN table_b AS b on a.id = b.id AND a.some_boolean = b.some_boolean')
        println 'Number of results returned: ' + rows.size()
        println rows.join('\n')
        println'Finished Executing Query 1'

    }

    def executeQuery2(Sql sql)
    {
        println'Starting Executing Query 2 - RIGHT JOIN'
        def rows =   sql.rows( 'SELECT a.id AS aId, a.name AS aName, b.id AS bId, b.name AS bName from  table_a AS a RIGHT JOIN table_b AS b on a.id = b.id AND a.some_boolean = b.some_boolean')
        println 'Number of results returned: ' + rows.size()
        rows.each{row->println(row)}
        println'Finished Executing Query 2'
    }

    def executeQuery3(Sql sql)
    {
        println'Starting Executing Query 3 - NO JOIN'
        def rows =   sql.rows( 'SELECT a.id AS aId, a.name AS aName, b.id AS bId, b.name AS bName from  table_a AS a, table_b AS b WHERE a.id = b.id AND a.some_boolean = b.some_boolean')
        println 'Number of results returned: ' + rows.size()
        rows.each{row->println(row)}
        println'Finished Executing Query 3'
    }
    def executeQuery4(Sql sql)
    {
        println'Starting Executing Query 4 - NO JOIN'
        def rows =   sql.rows( 'SELECT a.id AS aId, a.name AS aName, b.id AS bId, b.name AS bName from  table_a AS a, table_b AS b WHERE a.id = b.id AND a.some_boolean != b.some_boolean')
        println 'Number of results returned: ' + rows.size()
        rows.each{row->println(row)}
        println'Finished Executing Query 4'
    }


    def executeTask6(Sql sql)
    {
        println'Creating Table with String for Task 6'

        sql.execute('DROP TABLE IF EXISTS '+ 'table_task6')
        sql.execute('create table table_task6(name VARCHAR(255) NOT NULL)')

       List randomStrings=[]
            (1..25).each {
                String randomString = Helpers.getRandomStringValue()
                randomStrings.add(randomString)
            }

        println 'Populating table with 1.000.000 records'

        (1..1000000).each {
           def index = Helpers.getRandomInt(0,25)
            sql.execute 'insert into table_task6  values (?)', randomStrings[index]
        }

        println'Finished Executing Task 6 - 1.000.000 rows inserted '

    }

}

