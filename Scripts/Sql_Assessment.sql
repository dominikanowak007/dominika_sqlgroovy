/*        Task Three:

1. Construct a query using a MySQL client prompt or a MySQL editor (i.e. SQLYog or MySQL Workbench)
that joins data between snmp_manager.remote_ticket and ticketer.ticket for the last 24 hours (the db might not contain data for last 24 - please adjust to the actual data).
2. Expand your query so that you can join snmp_manager.active_alarm data into it as well as ticketer.ticket_status and ticketer.ticket_change.
Make sure you only query for the last 24 hours (the db might not contain data for last 24 - please adjust to the actual data).
You will benefit from reviewing the related domain classes to see how they ‘link’.
3. Expand your query so that you can join snmp_manager.general_trap_summary,
 snmp_manager.network_element and snmp_manager.map_details fields.  Make sure you pay attention to where conditions for time frame.
*/

SELECT *
FROM snmp_manager.remote_ticket  remote  JOIN ticketer.ticket ticket ON remote.ticket_identifier = ticket.id
WHERE  remote.create_date > date_sub(NOW(), INTERVAL 24 HOUR);

#################################################################################################################################################

 SELECT
 Ticket.id AS TicketIDfromTicket,
 TicketStatus.version AS Version,
 ChangeTicket.contents as TicketContents,
 ChangeTicket.header as TicketHeader,
 ChangeTicket.ticket_id AS TicketIDinChangeTicket,
 TicketStatus.ticket_id AS TicketIDfromTicketStatus,
 Ticket.create_date AS Ticket_creationDate,
 Ticket.closed_date AS Ticket_ClosedDate,
 Ticket.creator_id AS Ticket_CreatorID,
 Ticket.parent_id AS TicketParentID,
 ChangeTicket.active,
 RemoteTicket.ticket_identifier AS TicketIdentifierfromRemote ,
 RemoteTicket.parent_id AS Remotetbl_parentIdAlarmID,
 ActiveAlarm.id,
 RemoteTicket.id AS RemoteTicketID,
 RemoteTicket.ticket_identifier AS TicketIdentifier,
 ActiveAlarm.id AS AlarmID,
 ActiveAlarm.alarm_identifier AS AlarmIdentifier,
 ActiveAlarm.created_date,
 ActiveAlarm.network_element_id,
 ActiveAlarm.cleared
 FROM  snmp_manager.active_alarm ActiveAlarm
LEFT JOIN  snmp_manager.remote_ticket  RemoteTicket ON RemoteTicket.parent_Id=ActiveAlarm.id
LEFT JOIN ticketer.ticket Ticket ON RemoteTicket.ticket_identifier=Ticket.id
LEFT JOIN  ticketer.ticket_status TicketStatus ON Ticket.id = TicketStatus.ticket_id
LEFT JOIN ticketer.ticket_change ChangeTicket  ON Ticket.id = ChangeTicket.ticket_id
WHERE  RemoteTicket.create_date > date_sub(NOW(), INTERVAL 24 HOUR);

#################################################################################################################################################

SELECT *
FROM  snmp_manager.active_alarm ActiveAlarm
  LEFT JOIN snmp_manager.network_element NetElement ON ActiveAlarm.network_element_id =NetElement.id
  LEFT JOIN  snmp_manager.remote_ticket  RemoteTicket ON RemoteTicket.parent_Id=ActiveAlarm.id
  LEFT JOIN ticketer.ticket Ticket ON RemoteTicket.ticket_identifier=Ticket.id
  LEFT JOIN  ticketer.ticket_status TicketStatus ON Ticket.id = TicketStatus.ticket_id
  LEFT JOIN ticketer.ticket_change ChangeTicket  ON Ticket.id = ChangeTicket.ticket_id
  LEFT JOIN snmp_manager.general_trap_summary TrapSumm ON TrapSumm.network_element_id = NetElement.id  AND TrapSumm.id=ActiveAlarm.clearinggts_id
  LEFT JOIN snmp_manager.map_details MapDetails ON NetElement.map_details_id = MapDetails.id
WHERE  RemoteTicket.create_date > date_sub(NOW(), INTERVAL 24 HOUR);

#################################################################################################################################################

/*    Task 4

1.Construct a group query with a count(*) that returns the number of ‘snmp_manager.trap’ entries received in each day for the last week.
 Your result columns should look like |  DayOfWeek |  TrapCount |  or the like.

2. Construct a group query with a count(*)that shows an snmp_manager.network_element with ne_type=’Controller’
alongside a count of the number of ‘children’ it has.
 Again, use the domain definitions to help guide you for relationships.

*/


Select WEEKDAY( trap.date) as DayOfWeek , count(*) as TrapCount
from snmp_manager.trap trap
where trap.Date>date_sub(NOW(), INTERVAL 6 DAY)
group by  Cast(trap.date AS Date);

#################################################################################################################################################


Select Net.parent_id as ParentId, Count(*) as ChildrenNumber  from snmp_manager.network_element Net
where Net.parent_id in  
(
Select  Parent.id from snmp_manager.network_element Parent
where Parent.ne_type='Controller'
)
group by Net.parent_id

#################################################################################################################################################

/*
        Task 5

1. Construct a query in the snmp_manager database that returns all NetworkElement id and name with neType in (’Controller’,’Node’)
 that does outer joins to the following domains: MapDetails, AddressDetails, and PowerDetails.
  This means that the network element information should show for every row, even if information from any of the joining domains is not present.
   Also, include the name of the ‘parent’ of the Network Element.  A sample row could look like:

neId    |      neName    |   neParentName   |  mapLong    |  mapLat   |   addStreet   |  addJuris   |  powPoleId   |  powPoleType  |


*/

SELECT
  Net.id AS neID,
  Net.name AS neName,
  @parentName := (select P.name  from snmp_manager.network_element P where P.Id=Net.parent_id ) AS neParentName,
  Map.longitude AS mapLong,
  Map.latitude AS mapLat,
  Address.street_address AS addStreet,
  Address.jurisdiction AS addJuris,
  Power.id AS powPoleId,
  Power.pole_type AS powPoleType
FROM snmp_manager.network_element Net
  LEFT OUTER JOIN snmp_manager.map_details Map ON Net.map_details_id = Map.id
  LEFT OUTER JOIN snmp_manager.address_details Address ON Net.address_details_id = Address.id
  LEFT OUTER JOIN snmp_manager.power_details Power ON Net.power_details_id = Power.id
WHERE Net.ne_type  in ('Controller', 'Node')

/*

    Task 6

In your groovy console, create a table (be creative) that has a string field.  Add 1 million rows to it where the string field contains a value,
at random, from some static list of 25 string values.
Run a query like:  ‘SELECT DISTINCT your_string_column FROM your_table;
Notice that it takes a good bit of time (> 4 seconds or so).
Use the EXPLAIN statement to discover why.
Add an index to your_string_column.
Rerun the same query.  You should see the speed improve immensely… i.e. roughly 0 seconds.

*/

Explain Select distinct name  from table_task6;

Create index index_name On table_task6(name);